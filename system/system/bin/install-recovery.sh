#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:545ae9d0c418374624c0f15bbfc25d0d44b5e916; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:57812548315ecced4582b391452c5d9b76a5b7b9 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:545ae9d0c418374624c0f15bbfc25d0d44b5e916 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
